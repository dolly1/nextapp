import React from 'react';
import { shallow,configure,componentDidMount,mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'regenerator-runtime/runtime';
import Customer from '../../pages/Customers.js'
import {  Modal } from 'react-bootstrap'
import findDOMNode from 'react-dom'

configure({ adapter: new Adapter() });

describe('Customer', () => {
  var wrapper
  it('View data based on props',async (done)=> {
    const props = await Customer.getInitialProps()
    expect(props.data[2]).toEqual(
       { name: 'dolly',
         email: 'dolly@improwised.com',
         contact_no: '9228286922',
         country: 'India',
         pan_no: 'awjpe2345p',
         url: 'https://google.com',
         gender: 'female',
         languages: 'english',
         address: 'sdfsdfdsfsdf',
         id: 4 } );
    done();
  })

})