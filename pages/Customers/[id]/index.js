import { useRouter } from 'next/router'
import axios from 'axios'
import Header from '../../../components/Header'
import ViewCustomer from '../../../components/ViewCustomer'


const Index  = props => {
  const router = useRouter()
  const { id } = router.query
  return (
    <div>
      <Header  title="Customer"/>
      <ViewCustomer  data = {props.data} id={id}/>
    </div>
  )
}

Index.getInitialProps = async function({ query })  {
  const res = await axios.get(process.env.API_URL+'Customers/'+ query.id)
  const data = await res.data
  return {
    data: data
  }
}
export default Index