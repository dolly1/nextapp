import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import Header from '../components/Header'


const Home = () => (
  <div>
    <Header  title="About"/>
    <div className='example'>
      <h1 className='title'>About us page</h1>
    </div>
  </div>
)

export default Home
