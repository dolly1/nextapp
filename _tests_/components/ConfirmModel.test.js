import React from 'react';
import { shallow,configure,componentDidMount,mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'regenerator-runtime/runtime';
import App,{url} from '../../components/Customers';
import ConfirmModel from '../../components/ConfirmModel'
import {  Modal } from 'react-bootstrap'
import findDOMNode from 'react-dom'
import axios from 'axios';

configure({ adapter: new Adapter() });


jest.spyOn(axios, 'default');

const customers1=[{"name": "dolly",
      "email": "dolly@improwised.com",
      "contact_no": "9228286922",
      "country": "India",
      "pan_no": "awjpe2345p",
      "url": "https://google.com",
      "gender": "female",
      "languages": "english,gujarati",
      "address": "asdasd",
      "id": 4}];

const deleteurl = url + customers1.id;

const app = shallow(<App  data={customers1}/>);

const hmock = jest.fn(app.instance().handleClose)
const dmock = jest.fn(app.instance().deleteCustomer)

const wrapper = shallow(<ConfirmModel handleClose={hmock} deleteCustomer={dmock}  id={customers1.id}  customers={customers1}/>);


it('close modal window on #handleClose', ()=> {
 //cancel button test case
  wrapper.find('Button').at(0).simulate('click');
  expect(hmock).toHaveBeenCalled();
});


it('delete customers on #deleteCustomer',async (done)=> {
 //delete customer test case
  wrapper.find('Button').at(1).simulate('click');
  expect(dmock).toHaveBeenCalledWith(customers1.id,customers1);
  done();

});

