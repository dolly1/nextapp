import React, { Component } from 'react'
import { Button, Modal } from 'react-bootstrap'
import PropTypes from 'prop-types'

class ConfirmModel extends Component {

  constructor (props) {
    super(props)
    this.deleteCustomer = this.deleteCustomer.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleShow = this.handleShow.bind(this)
  }

  deleteCustomer () {
    if (this.props.customers) {
      this.props.deleteCustomer(this.props.id, this.props.customers)
    } else {
      this.props.deleteCustomer(this.props.id)
    }
  }

  handleClose () {
    this.props.handleClose()
  }

  handleShow () {
    this.props.handleShow()
  }

  render () {
    return (
      <Modal show={this.props.isOpen} onHide={this.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Are you sure ?</Modal.Title>
        </Modal.Header>
        <Modal.Body>Would you like to delete this customer?</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.handleClose}>Cancel</Button>
          <Button variant="primary" onClick={() => this.deleteCustomer()}> Ok </Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

ConfirmModel.propTypes = {
  deleteCustomer : PropTypes.func,
  handleShow : PropTypes.func,
  handleClose : PropTypes.func,
  customers : PropTypes.any,
  history : PropTypes.any,
  id : PropTypes.number,
  isOpen : PropTypes.any,
}

export default ConfirmModel
