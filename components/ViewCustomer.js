import React,{ Component } from 'react'
import axios from 'axios'
import { Button, Table,Container ,Row,Col } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import ConfirmModel from "./ConfirmModel"
import { faUser, faAngleDoubleLeft, faTrash,faEdit } from '@fortawesome/free-solid-svg-icons'
import PropTypes from 'prop-types'
import Router from 'next/router'
import Link from 'next/link'


export const url =process.env.API_URL+"Customers/";
class ViewCustomer extends Component {

  constructor(props) {

  super(props);
    this.state = {
      id: props.id,
      customers: props.data,
      pagename:'View Customer',
       isOpen: false
    }

    this.handleClose = this.handleClose.bind(this);
    this.handleShow = this.handleShow.bind(this);
  }

  handleClose() {
    this.setState({
      isOpen: false
    });
  }

  handleShow() {
    this.setState({
      isOpen: true
    });
  }

  async deleteCustomer(id) {
    axios.delete(url +id);
    Router.push('/Customers');
  }

  render()
  {
    const customers = this.state.customers;
    return(
      <Container className ="pt-5">

        {customers.length === 0 && (
          <Container className="text-center">
            <h2>No customer found at the moment</h2>
          </Container>
        )}

        {customers.length!== 0 && (
          <>
            <Row>
              <Col><h2><FontAwesomeIcon icon={faUser} />  {customers.name}&apos;s Profile  </h2></Col>
              <Col ><div className="float-right">
              <>
                <Link href="/Customers"  as="/Customers">
                <Button variant="info" className ="ml-2" ><FontAwesomeIcon icon={faAngleDoubleLeft} /> Back</Button></Link></>
              <>
                <Link href="/Customers/[id]/[Edit]"  as={`/Customers/${this.state.id}/edit`}>
                <Button variant="secondary" className ="ml-2" ><FontAwesomeIcon icon={faEdit} /> Edit </Button></Link></>

            <Button variant="danger" className ="ml-2" onClick={this.handleShow}><FontAwesomeIcon icon={faTrash} /> Delete</Button>
            </div>
            </Col>
            </Row>
            <hr/>

            <Table  borderless style={{fontSize: "20px",width:"50%"}}>
              <tbody>
                <tr><td>Name</td><td>{customers.name}</td></tr>
                <tr><td>Email</td><td><a href="mailto:{customers.email}">{customers.email}</a></td></tr>
                <tr><td>Contact No</td><td><a href="tel:{customers.contact_no}">{customers.contact_no}</a></td></tr>
                <tr><td >Gender</td><td>{customers.gender}</td></tr>
                <tr><td>Country</td><td>{customers.country}</td></tr>
                <tr><td>Languages</td><td>{customers.languages}</td></tr>
                <tr><td>Pan No</td><td>{customers.pan_no}</td></tr>
                <tr><td>URL</td><td><a href={customers.url}>{customers.url}</a></td></tr>
                <tr><td>Address</td><td>{customers.address}</td></tr>
              </tbody>
            </Table>
            <ConfirmModel isOpen={this.state.isOpen} handleClose={this.handleClose}  deleteCustomer={this.deleteCustomer} id={customers.id} />
          </>
        )}
      </Container>
    );
  }
}

ViewCustomer.propTypes = {
  match : PropTypes.any,
  history : PropTypes.any
}

export default ViewCustomer
