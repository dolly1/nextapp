import React from 'react';
import { shallow,configure,componentDidMount,mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'regenerator-runtime/runtime';
import App from '../../components/ViewCustomer';
import ConfirmModel from '../../components/ConfirmModel'
import {  Modal } from 'react-bootstrap'
import findDOMNode from 'react-dom'

configure({ adapter: new Adapter() });


const customers1=[{"name": "dolly",
  "email": "dolly@improwised.com",
  "contact_no": "9228286922",
  "country": "India",
  "pan_no": "awjpe2345p",
  "url": "https://google.com",
  "gender": "female",
  "languages": "english,gujarati",
  "address": "asdasd",
  "id": 4}];



const app = shallow(<App data={customers1}  id={4}/>);
it('view data on page load',async (done)=> {
    //check data table html
    expect(app.childAt(2).html()).toEqual('<table style="font-size:20px;width:50%" class="table table-borderless"><tbody><tr><td>Name</td><td></td></tr><tr><td>Email</td><td><a href="mailto:{customers.email}"></a></td></tr><tr><td>Contact No</td><td><a href="tel:{customers.contact_no}"></a></td></tr><tr><td>Gender</td><td></td></tr><tr><td>Country</td><td></td></tr><tr><td>Languages</td><td></td></tr><tr><td>Pan No</td><td></td></tr><tr><td>URL</td><td><a></a></td></tr><tr><td>Address</td><td></td></tr></tbody></table>');

    //Check edit link
    const ELink = app.find('Link').at(1);
    expect(ELink.props()["as"]).toEqual("/Customers/4/edit")

    done();
});


it('Check Back link',()=> {
  const Link = app.find('Link').at(0);
  expect(Link.length).toBe(1);
  expect(Link.props()["as"]).toEqual("/Customers")
});


it('Check delete link',()=> {
  app.find('Button').at(2).simulate('click');
  expect(app.state().isOpen).toBe(true);
  const wrapper = shallow(<ConfirmModel />);
  expect(wrapper.childAt(0).html()).toEqual('<div class="modal-header"><div class="modal-title h4">Are you sure ?</div><button type="button" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button></div>');
  expect(wrapper.childAt(1).html()).toEqual('<div class="modal-body">Would you like to delete this customer?</div>');
  expect(wrapper.childAt(2).html()).toEqual('<div class="modal-footer"><button type="button" class="btn btn-secondary">Cancel</button><button type="button" class="btn btn-primary"> Ok </button></div>');
});







