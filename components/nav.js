import React from 'react'
import Link from 'next/link'
import { Navbar, Nav, Container} from 'react-bootstrap';

const Linkurl = () => (
    <Container>
        <Navbar expand="lg" bg="light" variant="light" sticky="top">
          <Navbar.Brand href="/">Site Name</Navbar.Brand>
          <Navbar.Toggle  />
          <Navbar.Collapse className="justify-content-end" >
          <Nav className="justify-content-end" activeKey="/">
            <>
              <Link href="/" as="/">
                <a className="nav-link">Home</a>
              </Link>
              <Link href="/Customers" as="/Customers" >
                <a className="nav-link">Customer</a>
              </Link>
              <Link href="/about" as="/about">
                <a className="nav-link">About Us</a>
              </Link>
            </>
          </Nav>
        </Navbar.Collapse>
        </Navbar>
      </Container>
)

export default Linkurl
