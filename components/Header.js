import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import Linkurl from './nav'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

 const Header = props => (
 <Head>
      <title>{props.title}</title>
      <Linkurl />
  </Head>
)

export default Header
