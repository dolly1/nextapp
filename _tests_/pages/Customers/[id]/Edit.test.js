import React from 'react';
import { shallow,configure,componentDidMount,mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'regenerator-runtime/runtime';
import Edit from '../../../../pages/Customers/[id]/[Edit].js'
import {  Modal } from 'react-bootstrap'
import findDOMNode from 'react-dom'

configure({ adapter: new Adapter() });

describe('Edit', () => {
  var wrapper
  it('View data based on parameter',async (done)=> {
    const query = { id : '4' };
    const props = await Edit.getInitialProps({query})
    expect(props).toEqual( { data:
       { name: 'dolly',
         email: 'dolly@improwised.com',
         contact_no: '9228286922',
         country: 'India',
         pan_no: 'awjpe2345p',
         url: 'https://google.com',
         gender: 'female',
         languages: 'english',
         address: 'sdfsdfdsfsdf',
         id: 4 } });

    console.log(props);
    done();
  })
})