import React from 'react'
import axios from 'axios'
import Header from '../components/Header'
import Customers from '../components/Customers'


const Customer = props => {
  return(
  <>
    <Header title="Customers"/>
    <Customers  data = {props.data}/>
  </>
  )
}

Customer.getInitialProps = async function() {
  const res = await axios.get(process.env.API_URL+'Customers')
  const data = await res.data
  return {
    data: data
  }
}

export default Customer